#!/bin/sh

# If the first arg starts with a hyphen, prepend alisp to arguments.
if [ "${1#-}" != "$1" ]; then
	set -- alisp "$@"
fi

exec "$@"
