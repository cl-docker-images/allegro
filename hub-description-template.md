- [Supported Tags](#orgeb68f7e)
  - [Simple Tags](#orgd0bd26d)
  - [Shared Tags](#org12aa73a)
- [Quick Reference](#orgfad846c)
- [What is Allegro CL](#org3df7e4c)
- [How to use this image](#org2c47e68)
  - [Create a `Dockerfile` in your Allegro CL project](#org6a802eb)
  - [Run a single Common Lisp script](#org38611aa)
  - [Developing using SLIME](#orgfa101a1)
- [What's in the image?](#orgdb8e2b3)
- [Image variants](#orgf1d67b3)
  - [`%%IMAGE%%:<version>`](#orgd423831)
  - [`%%IMAGE%%:<version>-slim`](#org05ae9c4)
- [License](#orga7fc799)



<a id="orgeb68f7e"></a>

# Supported Tags


<a id="orgd0bd26d"></a>

## Simple Tags

INSERT-SIMPLE-TAGS


<a id="org12aa73a"></a>

## Shared Tags

INSERT-SHARED-TAGS


<a id="orgfad846c"></a>

# Quick Reference

-   **Allegro CL Home Page:** <https://franz.com/products/allegrocl/>
-   **Where to file Docker image related issues:** <https://gitlab.common-lisp.net/cl-docker-images/allegro>
-   **Maintained by:** [CL Docker Images Project](https://common-lisp.net/project/cl-docker-images)
-   **Supported platforms:** `linux/amd64`


<a id="org3df7e4c"></a>

# What is Allegro CL

From [Allegro CL's Home Page](https://franz.com/products/allegrocl/)

> Allegro CL ® is the most powerful dynamic object-oriented development system available today, and is especially suited to enterprise-wide, complex application development. Complex applications with billions of objects are now made easy with Allegro CL 10. The complexity of today's software applications and the explosion of data size are pervasive in all fields ranging from Life Sciences to Manufacturing to Financial Analytics. Allegro CL 10 is the most effective system for developing and deploying applications to solve these complex problems in the real world. For more information, contact info@franz.com.

Allegro CL is commercial software. These images include the express version of Allegro CL and are subject to the license agreement found at <https://franz.com/ftp/pub/legal/ACL-Express-20170301.pdf>.

In order to use these images, you must indicate your acceptance of the license by setting the environment variable `I_AGREE_TO_ALLEGRO_EXPRESS_LICENSE` to the value `yes`. This is step is not included in any of the examples below to help make sure you read and agree to the license.


<a id="org2c47e68"></a>

# How to use this image


<a id="org6a802eb"></a>

## Create a `Dockerfile` in your Allegro CL project

```dockerfile
FROM %%IMAGE%%:latest
COPY . /usr/src/app
WORKDIR /usr/src/app
CMD [ "alisp", "--batch", "-L", "./your-daemon-or-script.lisp" ]
```

You can then build and run the Docker image:

```console
$ docker build -t my-allegro-app
$ docker run -it --rm --name my-running-app my-allegro-app
```


<a id="org38611aa"></a>

## Run a single Common Lisp script

For many simple, single file projects, you may find it inconvenient to write a complete `Dockerfile`. In such cases, you can run a Lisp script by using the Allegro Docker image directly:

```console
$ docker run -it --rm --name my-running-script -v "$PWD":/usr/src/app -w /usr/src/app %%IMAGE%%:latest alisp -L ./your-daemon-or-script.lisp
```


<a id="orgfa101a1"></a>

## Developing using SLIME

[SLIME](https://common-lisp.net/project/slime/) provides a convenient and fun environment for hacking on Common Lisp. To develop using SLIME, first start the Swank server in a container:

```console
$ docker run -it --rm --name allegro-slime -p 127.0.0.1:4005:4005 -v /path/to/slime:/usr/src/slime -v "$PWD":/usr/src/app -w /usr/src/app %%IMAGE%%:latest alisp -L /usr/src/slime/swank-loader.lisp -e '(swank-loader:init)' -e '(swank:create-server :dont-close t :interface "0.0.0.0")'
```

Then, in an Emacs instance with slime loaded, type:

```emacs
M-x slime-connect RET RET RET
```


<a id="orgdb8e2b3"></a>

# What's in the image?

Allegro CL is commercial software. These images include the express version of Allegro CL and are subject to the license agreement found at <https://franz.com/ftp/pub/legal/ACL-Express-20170301.pdf>.

In order to use these images, you must indicate your acceptance of the license by setting the environment variable `I_AGREE_TO_ALLEGRO_EXPRESS_LICENSE` to the value `yes`. This is step is not included in any of the examples below to help make sure you read and agree to the license.


<a id="orgf1d67b3"></a>

# Image variants

This image comes in several variants, each designed for a specific use case.


<a id="orgd423831"></a>

## `%%IMAGE%%:<version>`

This is the defacto image. If you are unsure about what your needs are, you probably want to use this one. It is designed to be used both as a throw away container (mount your source code and start the container to start your app), as well as the base to build other images off of.

Some of these tags may have names like buster or stretch in them. These are the suite code names for releases of Debian and indicate which release the image is based on. If your image needs to install any additional packages beyond what comes with the image, you'll likely want to specify one of these explicitly to minimize breakage when there are new releases of Debian.

These images are built off the buildpack-deps image. It, by design, has a large number of extremely common Debian packages.

These images contain the Quicklisp installer, located at `/usr/local/share/common-lisp/source/quicklisp/quicklisp.lisp`. Additionally, there is a script at `/usr/local/bin/install-quicklisp` that will use the bundled installer to install Quicklisp. You can configure the Quicklisp install with the following environment variables:

-   **`QUICKLISP_DIST_VERSION`:** The dist version to use. Of the form yyyy-mm-dd. `latest` means to install the latest version (the default).
-   **`QUICKLISP_CLIENT_VERSION`:** The client version to use. Of the form yyyy-mm-dd. `latest` means to install the latest version (the default).
-   **`QUICKLISP_ADD_TO_INIT_FILE`:** If set to `true`, `(ql:add-to-init-file)` is used to add code to the implementation's user init file to load Quicklisp on startup. Not set by default.

Additionally, these images contain cl-launch to provide a uniform interface to running a Lisp implementation without caring exactly which implementation is being used (for instance to have uniform CI scripts).


<a id="org05ae9c4"></a>

## `%%IMAGE%%:<version>-slim`

This image does not contain the common packages contained in the default tag and only contains the minimal packages needed to run Allegro CL. Unless you are working in an environment where only this image will be used and you have space constraints, we highly recommend using the default image of this repository.


<a id="orga7fc799"></a>

# License

The license for Allegro CL Express Edition is found at: <https://franz.com/ftp/pub/legal/ACL-Express-20170301.pdf>.

NO WARRANTY. THE SOFTWARE IS LICENSED FREE OF CHARGE, AND THERE IS NO WARRANTY FOR THE SOFTWARE. FRANZ PROVIDES THE SOFTWARE "AS IS," AND FRANZ, ITS DISTRIBUTORS AND SUPPLIERS, AND ALL OTHER PERSONS WHO HAVE BEEN INVOLVED IN THE CREATION, PRODUCTION, OR DELIVERY OF THE SOFTWARE, DISCLAIM ALL CONDITIONS AND WARRANTIES OF ANY KIND, EITHER EXPRESS, IMPLIED, STATUTORY, OR OTHERWISE, INCLUDING, BUT NOT LIMITED TO, ANY CONDITIONS OR IMPLIED WARRANTIES OF MERCHANTABILITY, SATISFACTORY QUALITY, AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE RESULTS, QUALITY AND PERFORMANCE OF THE SOFTWARE IS WITH YOU AND YOUR DISTRIBUTEES. SHOULD THE SOFTWARE PROVE DEFECTIVE, YOU AND YOUR DISTRIBUTEES (AND NOT FRANZ) ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION. FRANZ MAKES NO WARRANTY OF NONINFRINGEMENT OF THE INTELLECTUAL PROPERTY RIGHTS OF THIRD PARTIES.

LIMITATION OF LIABILITY. UNDER NO CIRCUMSTANCES AND UNDER NO LEGAL THEORY, TORT, CONTRACT, OR OTHERWISE, SHALL FRANZ, ITS DISTRIBUTORS AND SUPPLIERS, OR ANY OTHER PERSON WHO HAS BEEN INVOLVED IN THE CREATION, PRODUCTION, OR DELIVERY OF THE SOFTWARE BE LIABLE TO YOU OR ANY OTHER PERSON FOR ANY GENERAL, DIRECT, INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL, OR OTHER DAMAGES OF ANY CHARACTER ARISING OUT OF THIS AGREEMENT OR THE USE OR INABILITY TO USE THE SOFTWARE, INCLUDING BUT NOT LIMITED TO PERSONAL INJURY, LOSS OF PROFITS, LOSS OF DATA, OUTPUT FROM THE SOFTWARE OR DATA BEING RENDERED INACCURATE, FAILURE OF THE SOFTWARE TO OPERATE WITH ANY OTHER PROGRAMS, DAMAGES FOR LOSS OF GOODWILL, BUSINESS INTERRUPTION, COMPUTER FAILURE OR MALFUNCTION, OR ANY AND ALL OTHER DAMAGES OR LOSSES OF WHATEVER NATURE, EVEN IF FRANZ HAS BEEN INFORMED OF THE POSSIBILITY OF SUCH DAMAGES.

The Dockerfiles used to build the images are licensed under BSD-2-Clause.

As with all Docker images, these likely also contain other software which may be under other licenses (such as Bash, etc from the base distribution, along with any direct or indirect dependencies of the primary software being contained).

As for any pre-built image usage, it is the image user's responsibility to ensure that any use of this image complies with any relevant licenses for all software contained within.